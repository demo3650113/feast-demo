import logging
from feast import FeatureStore, FeatureService

class FeatureStoreManager:
    def __init__(self, fs_yaml: str, store_path: str) -> None:
        self.fs_yaml = fs_yaml
        self.store_path = store_path
        self.store = None

    def connect_to_store(self) -> None:
        logging.info(f"Loading Feature Store from {self.fs_yaml}")
        self.store = FeatureStore(repo_path=self.store_path, fs_yaml_file=self.fs_yaml)
        logging.info("Connected to Feature Store")

    def create_feature_services(self, yaml_data: list[dict]) -> list[tuple[str, FeatureService]]:
        # Logic to create feature services using loaded yaml files
        fs_list = []
        for data in yaml_data:
          features_final = []
          
          fs_name = data['name']
          description=''
          tags=None
          owner=''
          if 'description' in data:
            description = data['description']
          if 'tags' in data:
            tags = data['tags']
          if 'owner' in data:
            owner = data['owner']

          logging.info(f'Configuring Feature Service {fs_name}...')
          for fv_name, features in data['features'].items():
              fv = self.store.get_feature_view(fv_name)
              features_final.append(fv[features])
              
          fs = FeatureService(name=fs_name, features=features_final, description=description, tags=tags, owner=owner)
          fs_list.append((fs_name, fs))

        return fs_list
    
    def save_feature_services(self, fs_list: list[tuple[str, FeatureService]]) -> None:
        # Logic to save feature services to the store
        for fs_name, fs in fs_list:
          logging.info(f'Saving Feature Service {fs_name}...')
          self.store.apply(fs)
          logging.info(f'Feature Service {fs_name} saved')

    def process_feature_services(self, yaml_data: list[dict]) -> None:
        fs_list = self.create_feature_services(yaml_data)
        self.save_feature_services(fs_list)
      
    def cleanup(self, yaml_data: list[dict]) -> None:
       """
       Removes feature services that do not have a Yaml file associated with them
       """
       fs_names = [data['name'] for data in yaml_data] # Get all the names from the feature services in the Yaml files
       feature_services = self.store.list_feature_services() # Get list of feature services from Feast
       print(fs_names)

       for fs_feast in feature_services:
          print(fs_feast.name)
          if fs_feast.name not in fs_names:
             logging.info(f'Deleting Feature Service {fs_feast.name} as it does not have a supporting Yaml configuration...')
             self.store.delete_feature_service(fs_feast)
