import argparse

def parse_args():
    parser = argparse.ArgumentParser(description="""
    This modules contains the functionality needed to create feature services from yaml files. This simplify the feature service creation process.\n
    Example usage: `pipenv run python model_feature_service_manager/main.py --fs_yaml feature_repo/staging_local_feature_store.yaml --store_path feature_repo --model feature_repo/models`
    """)
    parser.add_argument('--fs_yaml', type=str, help='Feature Store Yaml path', required=True)
    parser.add_argument('--model_dir', type=str, help='Path to model directory', required=True)
    parser.add_argument('--store_path', type=str, help='Path to feature store', required=True)
    return parser.parse_args()
