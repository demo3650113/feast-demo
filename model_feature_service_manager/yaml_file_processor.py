import os
import fnmatch
import yaml
import logging

def find_yaml_files(base_path: str) -> list[str]:
    yaml_files = []
    for root, dirs, files in os.walk(base_path):
        for file in files:
            if fnmatch.fnmatch(file, '*.yaml') or fnmatch.fnmatch(file, '*.yml'):
                logging.info("Found feature service configuration yaml file: " + os.path.join(root, file))
                yaml_files.append(os.path.join(root, file))
    return yaml_files

def read_yaml(yaml_file: str) -> dict():
    with open(yaml_file, 'r') as file:
        data = yaml.safe_load(file)
    return data

def find_and_process_yaml_files(base_path: str) -> list[dict]:
    # Logic to process YAML files
    all_yaml_files = find_yaml_files(base_path)
    data_list = []
    for yaml_path in all_yaml_files:
      data = read_yaml(yaml_path)
      if 'name' not in data:
          logging.error(f'Feature Service name not found in yaml file {yaml_path}')
          raise Exception
      data_list.append(data)
    return data_list
