import logging
import sys
from config_parser import parse_args
from feature_store_manager import FeatureStoreManager
from yaml_file_processor import find_and_process_yaml_files

"""
This modules contains the functionality needed to create feature services from yaml files. This simplify the feature service creation process.
Example usage: `pipenv run python model_feature_service_manager/main.py --fs_yaml feature_repo/staging_local_feature_store.yaml --store_path feature_repo --model feature_repo/models`
"""

def main():
    args = parse_args()
    logging.basicConfig(stream=sys.stdout, level=20, format='%(asctime)s - %(levelname)s - %(message)s')
    logging.info("Starting creation of feature services defined in yaml files")

    fs_manager = FeatureStoreManager(args.fs_yaml, args.store_path)
    fs_manager.connect_to_store()

    data_yaml_files = find_and_process_yaml_files(args.model_dir)
    fs_manager.process_feature_services(data_yaml_files)

    # fs_manager.cleanup(data_yaml_files)
    
    logging.info("Finished creation of feature services defined in yaml files")

if __name__ == "__main__":
    main()
