# Feast Demo

1. Run `make run-feast-local`
1. Run `make run-feast-prod`


The deploy-on-master job is only to see how CI is set up to move the changes to production. However, this does not work since we are using a file based system which them would make the path of the registry point to the CI system. This is not a problem when dealing with a real offline database