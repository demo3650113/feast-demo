from feast import (
    FeatureView,
    Field,
    FileSource
)
from feast.types import Int32
from datetime import timedelta
from entities import customer
import os

current_file_directory = os.path.dirname(os.path.abspath(__file__))

product = FileSource(
    path=f"{current_file_directory}/product_data.parquet",
    name='product',
    timestamp_field="SNAPSHOT_MONTH",
)

product_view = FeatureView(
    name="product",
    source=product,
    entities=[customer],
    ttl=timedelta(weeks=4),
    tags={"data": "product"},
    schema=[
        Field(name="NUM_UNIQUE_USERS",
                dtype=Int32,
                description="Number of unique users who have interacted with this product"),
        Field(name="NUM_ISSUES_CREATED", dtype=Int32, description="Number of issues created"),
        Field(name="NUM_FEATURES_USED", dtype=Int32, description="Number of features used"),
    ]
)
