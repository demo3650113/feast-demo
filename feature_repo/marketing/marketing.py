from feast import (
    FeatureView,
    Field,
    FileSource
)
from feast.types import Int32
from datetime import timedelta
from entities import customer
import os

current_file_directory = os.path.dirname(os.path.abspath(__file__))

marketing = FileSource(
    path=f"{current_file_directory}/marketing_data.parquet",
    name='marketing',
    timestamp_field="SNAPSHOT_MONTH",
)

marketing_view = FeatureView(
    name="marketing",
    source=marketing,
    entities=[customer],
    ttl=timedelta(weeks=4),
    tags={"data": "marketing"},
    schema=[
        Field(name="NUM_USERS_VISITED",
                dtype=Int32,
                description="Number of users visited the website"),
        Field(name="NUM_USERS_CLICKED", dtype=Int32, description="Number of users clicked on the website"),
    ]
)
