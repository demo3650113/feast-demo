from feast import (
    FeatureView,
    Field,
    FileSource
)
from feast.types import Int32
from datetime import timedelta
from entities import customer
import os

current_file_directory = os.path.dirname(os.path.abspath(__file__))

finance = FileSource(
    path=f"{current_file_directory}/finance_data.parquet", # Path to data relative to the feature repo
    name='finance',
    timestamp_field="SNAPSHOT_MONTH",
)

finance_view = FeatureView(
    name="finance",
    source=finance,
    entities=[customer],
    ttl=timedelta(weeks=4),
    tags={"data": "finance"},
    schema=[
        Field(name="REVENUE",
                dtype=Int32,
                description="Revenue of the company"),
        Field(name="CONTRACT_LENGTH", dtype=Int32, description="Contract length of the company"),
    ]
)
