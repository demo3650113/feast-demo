# Use an official Python runtime as a parent image
FROM python:3.10-slim

# Set the working directory in the container to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install pipenv
RUN pip install pipenv

# Install the Python packages specified in Pipfile
RUN pipenv install --deploy --ignore-pipfile  --verbose --system
