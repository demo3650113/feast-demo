from typing import List, Dict, Union
from feast import FeatureService, FeatureStore, FeatureView

def get_actionable_insight_descriptions(feature_service: FeatureService) -> Dict[str, str]:
   """ Given a Feast Feature Service, returns all the fields together with a nice formatted description
     that make said feature service and which have an actionable_insight tag.
     This is useful when generating insights to be used with the machine learning model output
   """
   return {f'{fv.name}__{cols.name}': cols.tags['actionable_insight'] for fs in [feature_service] 
    for fv in fs.feature_view_projections for cols in fv.to_proto().feature_columns if cols.tags and cols.tags['actionable_insight']}


def get_fields_from_feature_views(
    store: FeatureStore, fv_list: Union[List[str], List[FeatureView]]
) -> List[str]:
    """
    Given a list of feature views `List[FeatureView]` or feature views names `List[str]`, return a list of all the fields in the feature view.
    This list can then be used to call get_historical_features to get historical data from Feast.
    """
    if isinstance(fv_list[0], str):
        feature_views = [store.get_feature_view(fv) for fv in fv_list]
    else:
        feature_views = fv_list
    fields = [f"{fv.name}:{col.name}" for fv in feature_views for col in fv.features]
    return fields


def create_yaml_for_model_feature_service(
    store: FeatureStore,
    model_name: str,
    model_version: str,
    model_description: str,
    features: List[str],
    file_path: str,
) -> None:
    """
    Given a list of features with the naming convention {feature_view}__{feature_name} returns a yaml file that can be used to create the feature service in the Feast repository.
    If a feature does not match the naming convention it will be ignored.
    """
    from feast.errors import FeatureServiceNotFoundException
    import yaml

    feast_yaml = {}

    if model_name is None:
        raise ValueError("model_name cannot be empty")

    if model_version is None:
        raise ValueError("model_version cannot be empty")

    model_name_version = f"{model_name}:{model_version}"

    feast_yaml["name"] = model_name_version
    feast_yaml["description"] = model_description
    feast_yaml["features"] = {}

    # Get all feature views from the features
    # We need to remember some of these might be invalid
    feature_view_features = []
    invalid_features = []
    for feature in features:
        if len(feature.split("__")) == 2:
            feature_view_features.append(
                f"{feature.split('__')[0]}:{feature.split('__')[1].upper()}"
            )
        else:
            invalid_features.append(feature)

    if len(invalid_features) > 0:
        print(
            """Found features that have no feature views associated.\nThese likely come from custom feature transformations done outside Feast.\nMake sure to trace their origin and add the features that make them up manually to the YAML:"""
        )
        for feature in invalid_features:
            print(f"  - {feature}")

    features = [
        (
            feature.split("__")[0],
            f"{feature.split('__')[0]}:{feature.split('__')[1].upper()}",
        )
        for feature in features
        if len(feature.split("__")) == 2
    ]
    feature_views = list(set([feature[0] for feature in features]))
    feature_view_features = [feature[1] for feature in features]

    # Get all valid fields from the feature views gotten from the list in features
    feature_views_valid = []
    for fv in feature_views:
        try:
            feature_views_valid.append(store.get_feature_view(fv))
        except FeatureServiceNotFoundException:
            print(f"Feature view {fv} not found. It will be skipped")

    all_fields = get_fields_from_feature_views(store, feature_views_valid)

    # Get feature views that are not valid. They have a correct feature view but they are not valid feature names
    features_not_valid = list(set(feature_view_features).difference(all_fields))
    if len(features_not_valid) > 0:
        print(
            f"""The following features are not valid but have a valid feature view.\nThese likely come from custom feature transformations done outside Feast.\nMake sure to trace their origin and add the features that make them up manually to the YAML:"""
        )
        for feature in features_not_valid:
            print(f"  - {feature}")

    # Return the intersection of features views gotten to the valid feature views
    features_valid = sorted(list(set(feature_view_features).intersection(all_fields)))

    feast_yaml["features"] = {}
    for feature in features_valid:
        f = feature.split(":")
        if not f[0] in feast_yaml["features"].keys():
            feast_yaml["features"][f[0]] = []
        feast_yaml["features"][f[0]].append(f[1])

    with open(file_path, "w") as outfile:
        yaml.dump(feast_yaml, outfile, default_flow_style=False)
