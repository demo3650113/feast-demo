SHELL:=/bin/zsh

run-feast-local: # Runs Feast against your personal database. For local development
	@cd feature_repo && pipenv run feast --feature-store-yaml staging_feature_store.yaml apply
	@pipenv run python model_feature_service_manager/main.py --fs_yaml feature_repo/staging_feature_store.yaml --store_path feature_repo --model feature_repo/models

run-feast-prod: # Runs Feast against production database. This command is only for the demo as in a real implementation you would not be able to run this locally as it requires production credentials
	@cd feature_repo && pipenv run feast --feature-store-yaml production_feature_store.yaml apply
	@pipenv run python model_feature_service_manager/main.py --fs_yaml feature_repo/production_feature_store.yaml --store_path feature_repo --model feature_repo/models

ui-staging: # Runs the UI locally against your personal Feast database
	@cd feature_repo && pipenv run feast --feature-store-yaml staging_feature_store.yaml ui -r 1800 --port 8889

ui-local: # Runs the UI locally against the Feast production database
	@cd feature_repo && pipenv run feast --feature-store-yaml production_feature_store.yaml ui -r 1800 --port 8889
